<?php

namespace App\Http\Controllers;

use App\Http\Requests\Account\ActivateRequest;
use App\Http\Requests\Account\BalanceRequest;
use App\Http\Requests\Account\DeActivateRequest;
use App\Models\LoyaltyAccount;
use App\Repository\AccountRepositoryInterface;
use Illuminate\Http\Request;

class AccountController extends Controller
{

    private $accountRepository;

    public function __construct(AccountRepositoryInterface $accountRepository)
    {
        $this->accountRepository = $accountRepository;
    }


    public function create(Request $request)
    {
        return LoyaltyAccount::create($request->all());
    }

    public function activate(ActivateRequest $request)
    {
        $type = $request->type;
        $id = $request->id;
        $data = $this->accountRepository->activate($type, $id);
        return $data;
    }

    public function deactivate(DeActivateRequest $request)
    {
        $type = $request->type;
        $id = $request->id;
        $data = $this->accountRepository->deactivate($type, $id);
        return response()->json($data, 400);
    }

    public function balance(BalanceRequest $request)
    {
        $type = $request->type;
        $id = $request->id;
        $data = $this->accountRepository->balance($type, $id);
        return response()->json($data, 400);
    }
}
