<?php

namespace App\Http\Controllers;

use App\Http\Requests\Users\RegisterRequest;
use App\Models\User;
use App\Repository\UserRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    private $userRepository;


    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function register(RegisterRequest $request)
    {
        $input = $request->all();
        $data = $this->userRepository->register($input);

        return response()->json($data, 201);
    }

    public function login(Request $request)
    {
        $input = $request->all();
        $data = $this->userRepository->login();
        return $data;

    }

    public function logout(Request $request)
    {
        $request->user()->tokens()->delete();
        return ['message' => 'Logged out'];
    }
}
