<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoyaltyPoints\CancelRequest;
use App\Http\Requests\LoyaltyPoints\DepositRequest;
use App\Http\Requests\LoyaltyPoints\WithDrawRequest;
use App\Mail\LoyaltyPointsReceived;
use App\Models\LoyaltyAccount;
use App\Models\LoyaltyPointsTransaction;
use App\Repository\LoyaltyPointsRepositoryInterface;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class LoyaltyPointsController extends Controller
{
    private $loyaltyPointsRepository;

    public function __construct(LoyaltyPointsRepositoryInterface $loyaltyPointsRepository)
    {
        $this->loyaltyPointsRepository = $loyaltyPointsRepository;
    }

    public function deposit(DepositRequest $request)
    {
        $input = $request->all();
        $data = $this->loyaltyPointsRepository->deposit($input);
        return $data;
    }

    public function cancel(CancelRequest $request)
    {
        $input = $request->all();
        $data = $this->loyaltyPointsRepository->cancel($input);

        return $data;

    }

    public function withdraw(WithDrawRequest $request)
    {
        $input = $request->all();
        $data = $this->loyaltyPointsRepository->withdraw($input);
        return $data;


    }
}
