<?php

namespace App\Repository;
use Illuminate\Support\Collection;

interface UserRepositoryInterface
{

    /**
     * @param array $data
     * @return mixed
     */
    public function register(array $data): Collection;


    /**
     * @param array $data
     * @return mixed
     */
    public function login(array $data): Collection;
}
