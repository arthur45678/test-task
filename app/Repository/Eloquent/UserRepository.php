<?php

namespace App\Repository\Eloquent;

use App\Models\LoyaltyAccount;
use App\Models\User;
use App\Repository\UserRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserRepository extends  BaseRepository implements UserRepositoryInterface
{
    /**
     * UserRepository constructor.
     *
     * @param LoyaltyAccount $model
     */

    public function __construct(User $model)
    {
        parent::__construct($model);
    }


    /**
     * @param array $data
     * @return mixed
     */
    public function register($data): Collection
    {
        $result = [];
        $result['user'] = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        $result['token'] = $result['user']->createToken('apiToken')->plainTextToken;
        return $result;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function login($data): Collection
    {

        $user = $this->model->where('email', $data['email'])->first();

        if ($user && Hash::check($data['password'], $user->password)) {
            $token = $user->createToken('apiToken')->plainTextToken;
        }
        return response()->json(['message' => 'Bad credentials'], 401);

    }
}
