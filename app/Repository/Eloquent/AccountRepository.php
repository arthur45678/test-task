<?php

namespace App\Repository\Eloquent;

use App\Mail\AccountActivated;
use App\Mail\AccountDeactivated;
use App\Models\LoyaltyAccount;
use App\Models\LoyaltyPointsTransaction;
use App\Repository\AccountRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class AccountRepository extends BaseRepository implements AccountRepositoryInterface
{
    /**
     * UserRepository constructor.
     *
     * @param LoyaltyAccount $model
     */

    public function __construct(LoyaltyAccount $model)
    {
        parent::__construct($model);
    }

    /**
     * @param string $type
     * @param int $id
     * @return mixed
     */

    public function activate($type, $id): mixed
    {
        if (($type == 'phone' || $type == 'card' || $type == 'email') && $id != '') {
            if ($account = LoyaltyAccount::where($type, '=', $id)->first()) {
                if (!$account->active) {
                    $account->active = true;
                    $account->save();
                    $account->notify('Account restored');
                }
            } else {
                return response()->json(['message' => 'Account is not found'], 400);
            }
        } else {
            throw new \InvalidArgumentException('Wrong parameters');
        }

        return response()->json(['success' => true]);
    }

    /**
     * @param string $type
     * @param int $id
     * @return mixed
     */

    public function deactivate($type, $id): mixed
    {
        if (($type == 'phone' || $type == 'card' || $type == 'email') && $id != '') {
            if ($account = LoyaltyAccount::where($type, '=', $id)->first()) {
                if ($account->active) {
                    $account->active = false;
                    $account->save();
                    $account->notify('Account banned');
                }
            } else {
                return ['message' => 'Account is not found'];
            }
        } else {
            throw new \InvalidArgumentException('Wrong parameters');
        }

        return ['success' => true];
    }


    /**
     * @param string $type
     * @param int $id
     * @return mixed
     */
    public function balance($type, $id): mixed
    {
        if (($type == 'phone' || $type == 'card' || $type == 'email') && $id != '') {
            if ($account = LoyaltyAccount::where($type, '=', $id)->first()) {
                return ['balance' => $account->getBalance()];

            }
            return ['message' => 'Account is not found'];
        }
        throw new \InvalidArgumentException('Wrong parameters');

    }






}
