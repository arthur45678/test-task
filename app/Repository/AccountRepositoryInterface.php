<?php

namespace App\Repository;

use App\Models\LoyaltyPointsTransaction;
use Illuminate\Support\Collection;

interface AccountRepositoryInterface
{

    /**
     * @param string $type
     * @param int $id
     * @return mixed
     */
    public function activate($type, $id): mixed;

    /**
     * @param string $type
     * @param int $id
     * @return mixed
     */
    public function deactivate($type, $id): mixed;

    /**
     * @param string $type
     * @param int $id
     * @return mixed
     */
    public function balance($type, $id): mixed;



}
