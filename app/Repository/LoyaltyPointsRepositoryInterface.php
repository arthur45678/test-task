<?php

namespace App\Repository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

interface LoyaltyPointsRepositoryInterface
{

    /**
     * @param array $data
     * @return Log
     */
    public function deposit($data): Log;

    /**
     * @param array $data
     * @return mixed
     */
    public function cancel($data): mixed;

    /**
     * @param array $data
     * @return mixed
     */
    public function withdraw($data): mixed;

}
